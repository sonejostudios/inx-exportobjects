#!/usr/bin/env python
"""
export_objects - export a selection of objects, all layers or
                 sublayers to separate files.

Copyright (C) 2015-2016, su_v <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
# pylint: disable=missing-docstring
# pylint: disable=global-statement
# pylint: disable=too-many-public-methods
# pylint: disable=too-many-lines

# standard library
from copy import deepcopy
import os
import tarfile
import zipfile
try:
    # python2
    import StringIO
except ImportError:
    # python3
    from io import BytesIO
import calendar
import time
import tempfile
import shutil
import sys
import locale
from subprocess import Popen, PIPE
import re

# local library
import inkex
import simpletransform
import simplestyle
from pathmodifier import zSort


__version__ = '0.2'


SVG_NAMESPACE = inkex.NSS['svg']
SVG = "{%s}" % SVG_NAMESPACE

NS_DEFAULT = {None: SVG_NAMESPACE}

NS_MAP = dict(inkex.NSS)
NS_MAP.pop('svg', None)
NS_MAP.pop('ccOLD', None)
NS_MAP.pop('xml', None)
NS_MAP.update(NS_DEFAULT)

IDENT_MAT = [[1.0, 0.0, 0.0], [0.0, 1.0, 0.0]]
BASENAME = "step"
HAVE_CONVERT = False


# Possible workaround for encoding issues on Windows
# (see also bug #1518302)
ENCODING = sys.stdin.encoding
if ENCODING == 'cp0' or ENCODING is None:
    ENCODING = locale.getpreferredencoding()

# inkex.debug('Preferred encoding: {0}\n'.format(ENCODING))


def test_run(cmdlist):
    """Test run and return exit status."""
    myshell = True if os.name == 'nt' else False
    myproc = Popen(cmdlist, shell=myshell, stdout=PIPE, stderr=PIPE)
    myproc.communicate()
    rtcode = myproc.returncode
    return rtcode


if test_run(['convert', '-version']) == 0:
    HAVE_CONVERT = True


def run(command_format, stdin_str=None, verbose=False):
    """Run command"""
    # use for 'inkscape --shell' mode (hangs otherwise on Windows)
    if verbose:
        inkex.debug(command_format)
    out = err = None
    myproc = Popen(command_format, shell=False,
                   stdin=PIPE, stdout=PIPE, stderr=PIPE)
    # TODO python3: is this fix correct?
    if sys.version_info > (2,):
        if stdin_str is not None:
            stdin_str = stdin_str.encode()
    out, err = myproc.communicate(stdin_str)
    # TODO python3: is this fix correct?
    if sys.version_info > (2,):
        if out is not None:
            out = out.decode(ENCODING)
    if myproc.returncode == 0:
        return out
    elif err is not None:
        inkex.errormsg(err)


def run_with_shell_if_needed(command_format, stdin_str=None, verbose=False):
    """Run command with shell"""
    # use for ImageMagick 'convert' command ('shell=True' fails)
    if verbose:
        inkex.debug(command_format)
    out = err = None
    if os.name == 'nt':
        myshell = True
        mycommand = " ".join(command_format)
    else:
        myshell = False
        mycommand = command_format
    myproc = Popen(mycommand, shell=myshell,
                   stdin=PIPE, stdout=PIPE, stderr=PIPE)
    # TODO python3: is this fix correct?
    if sys.version_info > (2,):
        if stdin_str is not None:
            stdin_str = stdin_str.encode()
    out, err = myproc.communicate(stdin_str)
    # TODO python3: is this fix correct?
    if sys.version_info > (2,):
        if out is not None:
            out = out.decode(ENCODING)
    if myproc.returncode == 0:
        return out
    elif err is not None:
        inkex.errormsg(err)


def inkscape_shell(command_str, verbose=False):
    """Launch inkscape in shell mode and process commands in string."""
    opts = ['inkscape', '--shell']
    # FIXME: inkscape shell command fails on Windows with non-ASCII filenames
    stdout_str = run(opts, command_str, verbose)
    if verbose:
        return stdout_str


def query_bbox(svg_file, obj_id):
    """Query inkscape for x, y, width, height of object with id *obj_id*.

    parameters: svg file, id
    returns: list with x, y, width, height
    """
    opts = ['inkscape', '--shell']
    stdin_str = ""
    for arg in ['x', 'y', 'width', 'height']:
        stdin_str += '--file="{0}" --query-id={1} --query-{2}\n'.format(
            svg_file, obj_id, arg)
    stdout_str = run(opts, stdin_str, verbose=False)
    if stdout_str is not None:
        stdout_lst = stdout_str.split('>')
        if len(stdout_lst) >= 5:
            return stdout_lst[1:5]


def is_group(node):
    return node.tag == inkex.addNS('g', 'svg')


def is_layer(node):
    return (is_group(node) and
            node.get(inkex.addNS('groupmode', 'inkscape')) == 'layer')


def get_label(node):
    label = node.get(inkex.addNS('label', 'inkscape'), "")
    if sys.version_info < (3,):
        return label.encode(ENCODING)
    else:
        return label


def layers(document, rev=False):
    for node in document.getroot().iterchildren(reversed=rev):
        if is_layer(node):
            label = node.get(inkex.addNS('label', 'inkscape'), None)
            if sys.version_info < (3,):
                labelstring = label.encode(ENCODING)
            else:
                labelstring = label
            yield (labelstring, node)


def group_objs(document, rev=False):
    for node in document.getroot().iterchildren(reversed=rev):
        if is_group(node):
            yield node


def layer_objs(document, rev=False):
    for node in document.getroot().iterchildren(reversed=rev):
        if is_layer(node):
            yield node


def sublayer_objs(layer, rev=False):
    for node in layer.iterchildren(reversed=rev):
        if is_layer(node):
            yield node


def get_toplevel_group(node):
    """Return parent top-level group of node."""
    if is_group(node.getparent()):
        node = get_toplevel_group(node.getparent())
    return node


def get_toplevel_layer(node):
    """Return parent top-level layer of node."""
    if is_layer(node.getparent()):
        node = get_toplevel_layer(node.getparent())
    return node


def rel_to_abs_path(path):
    if not os.path.isabs(path):
        if os.name == 'nt':
            path = os.path.join(os.environ['USERPROFILE'], path)
        else:
            path = os.path.join(os.path.expanduser("~"), path)
    return path


def check_filepath(path):
    path = rel_to_abs_path(path)
    if os.path.isdir(path):
        return path
    else:
        inkex.errormsg('Export path "{0}" does not exist'.format(path))
        return None


def int_to_color(i):
    # pylint: disable=invalid-name
    r = ((i >> 24) & 255)
    g = ((i >> 16) & 255)
    b = ((i >> 8) & 255)
    color = "#%02x%02x%02x" % (r, g, b)
    opacity = (((i) & 255) / 255.0)
    return (color, opacity)


def set_pagecolor(node, color, opacity):
    node.set('pagecolor', color)
    node.set(inkex.addNS('pageopacity', 'inkscape'), str(round(opacity, 3)))


def get_element_by_id_from_doc(id_, document):
    path = '//*[@id="{0}"]'.format(id_)
    el_list = document.xpath(path, namespaces=inkex.NSS)
    if el_list:
        return el_list[0]
    else:
        return None


def create_svg_element_tree():
    doc_name = inkex.etree.QName('svg')
    doc_root = inkex.etree.Element(doc_name, nsmap=NS_MAP)
    doc_root.set('version', "1.1")
    comment_str = " Created with Inkscape (http://www.inkscape.org/) "
    comment_str += 'and {0} '.format(os.path.basename(__file__))
    doc_root.addprevious(inkex.etree.Comment(comment_str))
    return inkex.etree.ElementTree(doc_root)


def element_tree_to_string(doc):
    return inkex.etree.tostring(doc,
                                xml_declaration=True,
                                encoding="UTF-8",
                                pretty_print=True)


def wrap_copy_in_new_layer(node, label="Layer 1"):
    layer = inkex.etree.Element(inkex.addNS('g', 'svg'))
    layer.set(inkex.addNS('groupmode', 'inkscape'), "layer")
    if sys.version_info < (3,):
        layer.set(inkex.addNS('label', 'inkscape'), label.decode(ENCODING))
    else:
        layer.set(inkex.addNS('label', 'inkscape'), label)
    if node.getparent().getparent() is not None:
        mat = simpletransform.composeParents(node.getparent(), IDENT_MAT)
        simpletransform.applyTransformToNode(mat, layer)
    layer.append(deepcopy(node))
    return layer


def add_label_to_copy(node, doc, label):
    if sys.version_info < (3,):
        labelstring = label.decode(ENCODING)
    else:
        labelstring = label
    get_element_by_id_from_doc(
        node.get('id'), doc).set(
            inkex.addNS('label', 'inkscape'), labelstring)


def update_docname(doc, name):
    if sys.version_info < (3,):
        namestring = name.decode(ENCODING)
    else:
        namestring = name
    doc.getroot().set(inkex.addNS('docname', 'sodipodi'),
                      u'{0}.svg'.format(namestring))


def get_docname(doc):
    doc_name = doc.getroot().get(
        inkex.addNS('docname', 'sodipodi'), "unnamed.svg")
    if sys.version_info < (3,):
        return doc_name.encode(ENCODING)
    else:
        return doc_name


def clear_tmp(tmp_dir):
    shutil.rmtree(tmp_dir)


def init_tarfile(path, basename):
    return tarfile.open(
        os.path.join(path, '{0}.tar'.format(basename)), mode="w")


def add_to_tar(tar_file, document):
    if isinstance(tar_file, tarfile.TarFile):
        info = tarfile.TarInfo(name=get_docname(document))
        if sys.version_info < (3,):
            string = StringIO.StringIO()
            document.write(string)
            string.seek(0)
            info.size = len(string.buf)
        else:
            string = BytesIO()
            document.write(string)
            string.seek(0)
            info.size = len(string.getvalue())
        info.mtime = calendar.timegm(time.gmtime())
        tar_file.addfile(tarinfo=info, fileobj=string)


def init_zipfile(path, basename):
    return zipfile.ZipFile(
        os.path.join(path, '{0}.zip'.format(basename)), mode="w")


def add_to_zip(zip_file, document):
    if isinstance(zip_file, zipfile.ZipFile):
        info = zipfile.ZipInfo(filename=get_docname(document))
        info.date_time = time.localtime(time.time())[:6]
        info.compress_type = zipfile.ZIP_DEFLATED
        zip_file.writestr(info, element_tree_to_string(document))


def write_svg_file(path, document):
    if os.path.isdir(path):
        filepath = os.path.join(path, get_docname(document))
        if sys.version_info < (3,):
            with open(filepath, 'w') as output_file:
                output_file.write(element_tree_to_string(document))
                return filepath
        else:
            with open(filepath, 'wb') as output_file:
                output_file.write(element_tree_to_string(document))
                return filepath


def write_svg_to(export_type, export_target, document):
    if export_type == "files" and export_target:
        write_svg_file(export_target, document)
    elif export_type == "tar" and export_target is not None:
        add_to_tar(export_target, document)
    elif export_type == "zip" and export_target is not None:
        add_to_zip(export_target, document)


def copy_to_files(filepath, target):
    if os.path.isdir(target):
        shutil.copy(filepath, target)


def copy_to_zip(filepath, zip_file):
    if isinstance(zip_file, zipfile.ZipFile):
        zip_file.write(filepath, arcname=os.path.basename(filepath))


def copy_to_tar(filepath, tar_file):
    if isinstance(tar_file, tarfile.TarFile):
        tar_file.add(filepath, arcname=os.path.basename(filepath))


def copy_file_to(export_type, export_target, filepath):
    if export_type == "files" and export_target:
        copy_to_files(filepath, export_target)
    elif export_type == "tar" and export_target is not None:
        copy_to_tar(filepath, export_target)
    elif export_type == "zip" and export_target is not None:
        copy_to_zip(filepath, export_target)


def copy_filelist_to(file_list, export_type, export_target):
    counter = 0
    for filepath in file_list:
        if os.path.isfile(filepath):
            copy_file_to(export_type, export_target, filepath)
            counter += 1
    return counter


class EffectCompat(inkex.Effect):
    def __init__(self):
        inkex.Effect.__init__(self)

    # -----------------------------------------------------------------
    #
    # Copied from lp:~inkscape.dev/inkscape/extensions-svgunitfactor
    #
    #

    # Maintain separate dicts for 90 and 96 dpi to allow extensions to
    # implement support for legacy (or future) documents.
    __uuconv_90dpi = {
        'in': 90.0,
        'pt': 1.25,
        'px': 1.0,
        'mm': 3.543307086614174,
        'cm': 35.43307086614174,
        'm':  3543.307086614174,
        'km': 3543307.086614174,
        'pc': 15.0,
        'yd': 3240.0,
        'ft': 1080.0
    }

    __uuconv_96dpi = {
        'in': 96.0,
        'pt': 1.333333333333333,
        'px': 1.0,
        'mm': 3.779527559055119,
        'cm': 37.79527559055119,
        'm':  3779.527559055119,
        'km': 3779527.559055119,
        'pc': 16.0,
        'yd': 3456.0,
        'ft': 1152.0
    }

    # A dictionary of unit to user unit conversion factors.
    __uuconv = __uuconv_96dpi

    # New method: get_document_scale()
    # ================================
    # The new method supports arbitrary uniform scale factors and is
    # used in the unit conversion methods unittouu() and uutounit().

    def match_uuconv(self, val, eps=0.01):
        """Fuzzy matching of value to one of the known unit factors."""
        match = None
        for key in self.__uuconv:
            if inkex.are_near_relative(self.__uuconv[key], val, eps):
                match = self.__uuconv[key]
        return match or val

    def split_svg_length(self, string, percent=False):
        """Split SVG length string into float and unit string."""
        # pylint: disable=invalid-name
        param = unit = None
        if percent:
            unit_list = '|'.join(list(self.__uuconv.keys()) + ['%'])
        else:
            unit_list = '|'.join(list(self.__uuconv.keys()))
        param_re = re.compile(
            r'(([-+]?[0-9]+(\.[0-9]*)?|[-+]?\.[0-9]+)([eE][-+]?[0-9]+)?)')
        unit_re = re.compile(
            '({0})$'.format(unit_list))
        if string is not None:
            p = param_re.match(string)
            u = unit_re.search(string)
            if p:
                try:
                    param = float(p.string[p.start():p.end()])
                except (KeyError, ValueError):
                    pass
            if u:
                try:
                    unit = u.string[u.start():u.end()]
                except KeyError:
                    pass
        return (param, unit)

    def get_page_dimension(self, attribute, percent=True):
        """Retrieve value for SVGRoot attribute passed as argument.

        Return list of float and string.
        """
        string = self.document.getroot().get(attribute, "100%")
        val, unit = self.split_svg_length(string, percent=percent)
        dimension = 100.0 if val is None else val
        dimension_unit = 'px' if unit is None else unit
        return (dimension, dimension_unit)

    def get_viewbox(self):
        """Retrieve value for viewBox from current document.

        Return list of 4 floats or None.
        """
        viewbox_attribute = self.document.getroot().get('viewBox', None)
        viewbox = []
        if viewbox_attribute:
            try:
                viewbox = list(float(i) for i in viewbox_attribute.split())
            except ValueError:
                pass
        if len(viewbox) != 4 or (viewbox[2] < 0 or viewbox[3] < 0):
            viewbox = None
        return viewbox

    def check_viewbox(self, width, w_unit, height, h_unit):
        """Verify values for SVGRoot viewBox width and height.

        Return list of 2 floats.
        """
        viewbox = self.get_viewbox()
        if viewbox is None:
            # If viewBox attribute is missing or invalid: calculate
            # viewBox dimensions corresponding to width, height
            # attributes. Treat '%' as special case, else assume 96dpi.
            vb_width = (width if w_unit == '%' else
                        self.__uuconv[w_unit] * width)
            vb_height = (height if h_unit == '%' else
                         self.__uuconv[h_unit] * height)
        else:
            vb_width, vb_height = viewbox[2:4]
        return (vb_width, vb_height)

    def get_aspectratio(self):
        """Get preserveAspectRatio from SVGRoot.

        Return list of 2 strings.
        """
        preserve_aspect_ratio = self.document.getroot().get(
            'preserveAspectRatio', "xMidYMid meet")
        try:
            return preserve_aspect_ratio.split()
        except ValueError:
            # Nothing to split: Set <meetOrSlice> parameter to default
            # 'meet'.  Note that the <meetOrSlice> parameter is ignored
            # if <align> parameter is 'none'.
            return (preserve_aspect_ratio, "meet")

    def get_aspectratio_scale(self, width, w_unit, height, h_unit,
                              vb_width, vb_height):
        """Calculate offset, scale based on SVGRoot preserveAspectRatio.

        Return list of 4 floats.
        """
        # pylint: disable=too-many-arguments
        # pylint: disable=too-many-locals
        x_offset = y_offset = 0.0
        scale = 1.0
        aspect_align, aspect_clip = self.get_aspectratio()
        if aspect_align == 'none':
            # TODO: implement support for non-uniform scaling
            # based on preserveAspectRatio attribute.
            pass
        else:
            width = vb_width * (width / 100.0) if w_unit == '%' else width
            height = vb_height * (height / 100.0) if h_unit == '%' else height
            vb_width = width if vb_width == 0 else vb_width
            vb_height = height if vb_width == 0 else vb_height
            scale_x = width / vb_width
            scale_y = height / vb_height
            # Force uniform scaling based on <meetOrSlice> parameter
            scale = (aspect_clip == "meet" and
                     min(scale_x, scale_y) or
                     max(scale_x, scale_y))
            # calculate offset based on <align> parameter
            align_x = aspect_align[1:4]
            align_y = aspect_align[5:8]
            offset_factor = {'Min': 0.0, 'Mid': 0.5, 'Max': 1.0}
            try:
                # TODO: verify units of calculated offsets
                x_offset = round(
                    offset_factor[align_x] * (width - vb_width*scale), 3)
                y_offset = round(
                    offset_factor[align_y] * (height - vb_height*scale), 3)
            except KeyError:
                pass
        return (x_offset, y_offset, scale, scale)

    def apply_viewbox(self):
        """Return offset (x, y) and scale for width, height of SVGRoot."""

        width, w_unit = self.get_page_dimension('width')
        height, h_unit = self.get_page_dimension('height')
        vb_width, vb_height = self.check_viewbox(
            width, w_unit, height, h_unit)
        x_offset, y_offset, scale_x, scale_y = self.get_aspectratio_scale(
            width, w_unit, height, h_unit, vb_width, vb_height)

        # Treat '%' as special case, else apply scale to conversion
        # factor from __uuconv.  Use match_uuconv() to allow precision
        # tolerance of the document's page dimensions.
        w_scale, h_scale = [(scale if unit == '%' else
                             self.match_uuconv(self.__uuconv[unit] * scale))
                            for unit, scale in (
                                (w_unit, scale_x), (h_unit, scale_y))]

        return (x_offset, y_offset, w_scale, h_scale)

    def get_document_scale(self):
        """Return document scale factor.

        Calculate scale based on these SVGRoot attributes:
        'width', 'height', 'viewBox', 'preserveAspectRatio'
        """
        unitfactor = self.__uuconv['px']  # fallback
        width_unitfactor, height_unitfactor = self.apply_viewbox()[2:4]
        return width_unitfactor or height_unitfactor or unitfactor

    # Unit conversion tools
    # =====================
    # Methods to assist unit handling in extensions / derived classes.

    def unittouu(self, string):
        """Return userunits given a string representation in units.

        Return float or None.
        """
        val = None
        if string is not None:
            val, unit = self.split_svg_length(string)
            if val is None:
                val = 0.0
            if unit is None:
                val /= self.get_document_scale()  # Assume default 'px'.
            elif unit in self.__uuconv.keys():
                val *= (self.__uuconv[unit] / self.get_document_scale())
        return val

    def uutounit(self, val, unit):
        """Return value in userunits converted to other units."""
        return val / (self.__uuconv[unit] / self.get_document_scale())

    def switch_uuconv(self, dpi):
        """Allow extensions to override internal resolution."""
        if dpi == '90':
            self.__uuconv = self.__uuconv_90dpi
        elif dpi == '96':
            self.__uuconv = self.__uuconv_96dpi
        else:  # unchanged
            pass

    #
    #
    # Copied from lp:~inkscape.dev/inkscape/extensions-svgunitfactor
    #
    # -----------------------------------------------------------------


class ExportObjects(EffectCompat):
    def __init__(self):
        # pylint: disable=too-many-statements
        EffectCompat.__init__(self)

        # instance attributes
        self.objects = []
        self.commonlayers = []
        self.incremental_bbox = []

        # support arbitrary document scale (EffectCompat)
        self.documentscale = None
        self.viewboxheight = None

        self.OptionParser.add_option("--export_format",
                                     action="store", type="string",
                                     dest="export_format", default="svg",
                                     help="Export format")
        self.OptionParser.add_option("--export_type",
                                     action="store", type="string",
                                     dest="export_type", default="files",
                                     help="Export type")
        # export scope
        self.OptionParser.add_option("--nb_scope",
                                     action="store", type="string",
                                     dest="nb_scope",
                                     help="Export scope")
        # selection
        self.OptionParser.add_option("--selection_other",
                                     action="store", type="inkbool",
                                     dest="selection_other", default=False,
                                     help="Include unselected in template")
        self.OptionParser.add_option("--selection_fitpage",
                                     action="store", type="inkbool",
                                     dest="selection_fitpage", default=False,
                                     help="Fit page to object")
        # layers
        self.OptionParser.add_option("--layer_other",
                                     action="store", type="inkbool",
                                     dest="layer_other",
                                     default=False,
                                     help="Include common layers in template")
        self.OptionParser.add_option("--layer_list",
                                     action="store", type="string",
                                     dest="layer_list",
                                     default="",
                                     help="Layers included in all exports")
        self.OptionParser.add_option("--layer_visible",
                                     action="store", type="inkbool",
                                     dest="layer_visible", default=False,
                                     help="Export visible layers only")
        self.OptionParser.add_option("--layer_reset",
                                     action="store", type="inkbool",
                                     dest="layer_reset", default=True,
                                     help="Reset layer opacity")
        self.OptionParser.add_option("--layer_fitpage",
                                     action="store", type="inkbool",
                                     dest="layer_fitpage", default=False,
                                     help="Fit page to layer")
        # sublayers
        self.OptionParser.add_option("--sublayer_other",
                                     action="store", type="inkbool",
                                     dest="sublayer_other",
                                     default=False,
                                     help="Include common layers in template")
        self.OptionParser.add_option("--sublayer_list",
                                     action="store", type="string",
                                     dest="sublayer_list",
                                     default="",
                                     help="Layers included in all exports")
        self.OptionParser.add_option("--sublayer_visible",
                                     action="store", type="inkbool",
                                     dest="sublayer_visible", default=False,
                                     help="Export visible layers only")
        self.OptionParser.add_option("--sublayer_reset",
                                     action="store", type="inkbool",
                                     dest="sublayer_reset", default=True,
                                     help="Reset layer opacity")
        self.OptionParser.add_option("--sublayer_scope",
                                     action="store", type="string",
                                     dest="sublayer_scope",
                                     default="sublayer_current",
                                     help="Sublayer scope")
        self.OptionParser.add_option("--sublayer_level",
                                     action="store", type="int",
                                     dest="sublayer_level",
                                     default=1,
                                     help="Sublayer level")
        self.OptionParser.add_option("--sublayer_fitpage",
                                     action="store", type="inkbool",
                                     dest="sublayer_fitpage", default=False,
                                     help="Fit page to sublayer")
        # export filename, path, counter
        self.OptionParser.add_option("--export_path",
                                     action="store", type="string",
                                     dest="export_path", default="splitgroup",
                                     help="Export directory")
        self.OptionParser.add_option("--basename",
                                     action="store", type="string",
                                     dest="basename", default="step",
                                     help="Export directory")
        self.OptionParser.add_option("--counter_zfill",
                                     action="store", type="int",
                                     dest="counter_zfill", default=5,
                                     help="Counter format (number of digits)")
        self.OptionParser.add_option("--counter_start",
                                     action="store", type="int",
                                     dest="counter_start", default=1,
                                     help="Counter start value")
        self.OptionParser.add_option("--incremental",
                                     action="store", type="inkbool",
                                     dest="incremental", default=False,
                                     help="Incremental export")
        # content options
        self.OptionParser.add_option("--with_defs",
                                     action="store", type="inkbool",
                                     dest="with_defs", default=False,
                                     help="Include defs in template")
        self.OptionParser.add_option("--reverse",
                                     action="store", type="inkbool",
                                     dest="reverse", default=False,
                                     help="Reverse z-order")
        self.OptionParser.add_option("--background",
                                     action="store", type="string",
                                     dest="background", default="default",
                                     help="Document background")
        self.OptionParser.add_option("--background_color",
                                     action="store", type="int",
                                     dest="background_color", default=-256,
                                     help="Custom background color")
        self.OptionParser.add_option("--fitpage_margin",
                                     action="store", type="float",
                                     dest="fitpage_margin", default=0.0,
                                     help="Margin for fit page")
        self.OptionParser.add_option("--fitpage_margin_unit",
                                     action="store", type="string",
                                     dest="fitpage_margin_unit",
                                     default="px",
                                     help="Margin unit for fit page")
        # file formats
        self.OptionParser.add_option("--svg_plain",
                                     action="store", type="inkbool",
                                     dest="svg_plain", default=False,
                                     help="Save as Plain SVG")
        self.OptionParser.add_option("--svg_text_outline",
                                     action="store", type="inkbool",
                                     dest="svg_text_outline", default=False,
                                     help="Outline text in Plain SVG")
        self.OptionParser.add_option("--png_resolution",
                                     action="store", type="int",
                                     dest="png_resolution", default=96,
                                     help="PNG export resolution")
        self.OptionParser.add_option("--gif_interlace",
                                     action="store", type="inkbool",
                                     dest="gif_interlace", default=False,
                                     help="GIF interlace")
        self.OptionParser.add_option("--jpg_interlace",
                                     action="store", type="inkbool",
                                     dest="jpg_interlace", default=False,
                                     help="JPEG interlace")
        self.OptionParser.add_option("--jpg_quality",
                                     action="store", type="int",
                                     dest="jpg_quality", default=90,
                                     help="JPEG quality")
        self.OptionParser.add_option("--pdf_text_outline",
                                     action="store", type="inkbool",
                                     dest="pdf_text_outline", default=False,
                                     help="Outline text in PDF")
        self.OptionParser.add_option("--pdf_filter_raster",
                                     action="store", type="inkbool",
                                     dest="pdf_filter_raster", default=True,
                                     help="Rasterize filter effects in PDF")
        self.OptionParser.add_option("--pdf_resolution",
                                     action="store", type="int",
                                     dest="pdf_resolution", default=96,
                                     help="PDF filter resolution")
        # GIF animation
        self.OptionParser.add_option("--anim_gif",
                                     action="store", type="inkbool",
                                     dest="anim_gif", default=False,
                                     help="Create animated GIF image")
        self.OptionParser.add_option("--anim_gif_filename",
                                     action="store", type="string",
                                     dest="anim_gif_filename",
                                     default="",
                                     help="GIF anim: filename")
        self.OptionParser.add_option("--anim_gif_delay",
                                     action="store", type="int",
                                     dest="anim_gif_delay", default=20,
                                     help="GIF anim: delay between frames")
        self.OptionParser.add_option("--anim_gif_loop",
                                     action="store", type="int",
                                     dest="anim_gif_loop", default=1,
                                     help="GIF anim: number of iterations")
        self.OptionParser.add_option("--anim_gif_dispose",
                                     action="store", type="string",
                                     dest="anim_gif_dispose", default="0",
                                     help="GIF anim: image disposal method")
        self.OptionParser.add_option("--anim_gif_optim",
                                     action="store", type="inkbool",
                                     dest="anim_gif_optim", default=False,
                                     help="Optimize animated GIF with IM")
        self.OptionParser.add_option("--anim_gif_cmd",
                                     action="store", type="inkbool",
                                     dest="anim_gif_cmd", default=False,
                                     help="Save command to create anim GIF")
        # tab
        self.OptionParser.add_option("--tab",
                                     action="store", type="string",
                                     dest="tab",
                                     help="The selected UI-tab")

    # ----- support arbitrary document units

    def get_inkscape_version(self):
        """Return Inkscape version extracted from version attribute."""
        # NOTE: r14965 changed how this attribute is updated:
        # Inkscape no longer updates the attribute on load but on save.
        # New files thus don't have such an attribute anymore.
        # quick workaround: default to (assumed) 0.92
        inkscape_version_string = self.document.getroot().get(
            inkex.addNS('version', 'inkscape'), "0.92")
        version_match = re.compile(
            r'(0.48|0.48\+devel|0.91pre|0.91\+devel|0.91|0.92)')
        match_result = version_match.search(inkscape_version_string)
        if match_result is not None:
            inkscape_version = match_result.groups()[0]
        else:
            inkex.errormsg("Failed to retrieve Inkscape version.\n")
            inkscape_version = "Unknown"
        return inkscape_version

    def set_dpi(self, dpi):
        """Support 90 and 96 dpi."""
        if dpi == 'default':
            ink_ver = self.get_inkscape_version()
            # TODO: compare numerically to >= 0.92 for future releases
            if ink_ver == '0.91+devel' or ink_ver == '0.92':
                self.switch_uuconv('96')
            else:  # Assume older version with 90dpi
                self.switch_uuconv('90')
        else:
            self.switch_uuconv(dpi)
        # reset information about scale saved by overloaded methods
        self.documentscale = None
        self.viewboxheight = None

    # overload method from base class to store scale as instance attribute
    def get_document_scale(self):
        """Return document scale."""
        if self.documentscale is None:
            unitfactor = 1.0  # fallback 'px'
            width_unitfactor, height_unitfactor = self.apply_viewbox()[2:4]
            self.documentscale = (width_unitfactor or
                                  height_unitfactor or
                                  unitfactor)
        return self.documentscale

    # ----- export selection: fit to page

    def get_page_area_units(self):
        """Return units defined in <svg> width, height attributes."""
        w_unit = self.get_page_dimension('width', percent=False)[1]
        h_unit = self.get_page_dimension('height', percent=False)[1]
        return w_unit, h_unit

    def fit_page_to_drawing(self, document, node):
        """Fit page of document to drawing extent."""
        pass

    def offset_selection_obj(self, document, obj, mat):
        """Offset exported object to new origin."""
        if is_group(obj.getparent()):
            if self.options.selection_other:
                for group in group_objs(document):
                    simpletransform.applyTransformToNode(mat, group)
            else:
                simpletransform.applyTransformToNode(mat, obj.getparent())
        else:
            # TODO: investigate what is needed for defs
            simpletransform.applyTransformToNode(mat, obj)

    def offset_layer_obj(self, document, obj, mat):
        """Offset exported layer(s) to new origin."""
        if self.options.layer_other or self.options.sublayer_other:
            for layer in layer_objs(document):
                simpletransform.applyTransformToNode(mat, layer)
        else:
            simpletransform.applyTransformToNode(mat, obj)

    def add_margin(self, bbox):
        """Add margin to bbox, modify bbox list in-place."""
        scale = self.unittouu('1{}'.format(self.options.fitpage_margin_unit))
        margin = float(self.options.fitpage_margin) * scale
        # allow negative margin, check for min value
        if margin > 0 or 2*abs(margin) < min(bbox[2:4]):
            bbox[0] -= margin
            bbox[1] -= margin
            bbox[2] += 2 * margin
            bbox[3] += 2 * margin

    def current_bbox(self, node):
        """Return current bbox as new list, update incremental_bbox."""
        # get visual bbox of node from inkscape via command line
        scale = self.unittouu('1px')
        bbox = [float(s) * scale
                for s in query_bbox(self.svg_file, node.get('id'))]
        if self.options.incremental:
            # merge object bbox with incremental bbox
            if not len(self.incremental_bbox):
                self.incremental_bbox = 4 * [0]  # init incremental_bbox
            else:
                extent = self.incremental_bbox
                # merge stored bbox with current object's bbox
                min_x = min(extent[0], bbox[0])
                min_y = min(extent[1], bbox[1])
                max_w = max(extent[0] + extent[2], bbox[0] + bbox[2]) - min_x
                max_h = max(extent[1] + extent[3], bbox[1] + bbox[3]) - min_y
                # reset bbox with merged values
                bbox = [min_x, min_y, max_w, max_h]
            # store incremented bbox
            for i in range(4):
                self.incremental_bbox[i] = bbox[i]
        if self.options.fitpage_margin:
            self.add_margin(bbox)
        return bbox

    def fit_page_to_obj(self, document, node):
        """Fit page of document to exported object."""

        # Options to fit page of exported SVG doc to node:
        #
        # 1. get bounding box of node
        # 2. a) set document width, height, viewBox to node bbox size
        #       move object (or top-level parent group) to new origin
        # 2. b) set document width, height, viewBox to node bbox size
        #       adjust viewBox offset based on bbox offset

        # root of export document
        root = document.getroot()
        # get object in export document
        obj = get_element_by_id_from_doc(node.get('id'), document)
        # transform options: 'move_object' | 'offset_viewbox'
        fit_mode = 'move_object'
        # get current bbox
        bbox = self.current_bbox(node)
        if fit_mode == 'move_object':
            # set viewBox dimensions from node bbox
            root.set('viewBox', '0 0 {} {}'.format(*bbox[2:4]))
            # transform node to new origin (invert bbox offset)
            mat = simpletransform.parseTransform(
                'translate({},{})'.format(*[-1 * v for v in bbox[0:2]]))
            if self.options.nb_scope == '"selection"':
                self.offset_selection_obj(document, obj, mat)
            elif self.options.nb_scope in ('"layer"', '"sublayer"'):
                self.offset_layer_obj(document, obj, mat)
        elif fit_mode == 'offset_viewbox':
            # set viewBox offset, dimensions from node bbox
            root.set('viewBox', '{} {} {} {}'.format(*bbox))
        else:
            pass
        # adjust document width, height using original units
        if fit_mode in ('move_object', 'offset_viewbox'):
            w_unit, h_unit = self.get_page_area_units()
            root.set('width', '{}{}'.format(
                self.uutounit(bbox[2], w_unit), w_unit))
            root.set('height', '{}{}'.format(
                self.uutounit(bbox[3], h_unit), h_unit))

    # ----- export objects

    def get_common_layers(self, layerlist):
        """Return list of common layers to be included in template."""
        if not self.commonlayers:
            common_layers = layerlist.split(',')
            for (label, node) in layers(self.document):
                if label in common_layers:
                    self.commonlayers.append(node)
        return self.commonlayers

    def check_basename(self):
        """Check basename, use fallback name if empty."""
        if sys.version_info < (3,):
            basename = self.options.basename.decode(ENCODING)
        else:
            basename = self.options.basename
        if not basename:
            self.options.basename = BASENAME
        elif os.name == 'nt':
            # FIXME: crude workaround for inkscape shell command
            # failing on Windows with non-ASCII filenames
            # FIXME python3
            self.options.basename = basename.encode('ascii', 'ignore')

    def check_counter(self):
        """Check counter format, increase zfill if needed."""
        max_steps = 10**self.options.counter_zfill - self.options.counter_start
        if max_steps < len(self.objects):
            self.options.counter_zfill += 1
            self.check_counter()

    def out_info(self, out_count):
        """Return string reporting number of exported files."""
        if self.options.nb_scope == '"layer"':
            scope = 'layers'
        elif self.options.nb_scope == '"sublayer"':
            scope = 'sublayers'
        else:
            scope = 'objects'
        info = '\n{0} {1} exported to {2} {3} files.'.format(
            len(self.objects),
            scope,
            out_count,
            self.options.export_format.upper())
        return info

    def copy_doc_empty(self):

        def copy_page_node(tag):
            node = self.document.getroot().find(tag)
            if node is not None:
                page.append(deepcopy(node))

        template = create_svg_element_tree()
        page = template.getroot()
        attributes = self.document.getroot().attrib
        for name in attributes:
            page.set(name, attributes[name])
        copy_page_node(inkex.addNS('namedview', 'sodipodi'))
        copy_page_node(inkex.addNS('metadata', 'svg'))
        if self.options.with_defs:
            copy_page_node(inkex.addNS('defs', 'svg'))
        return template

    def copy_doc_with_unselected(self):
        template = deepcopy(self.document)
        if len(self.selected) == 1:
            group = get_element_by_id_from_doc(self.options.ids[0], template)
            if group is not None:
                for child in group.iterchildren():
                    group.remove(child)
        else:
            for obj_id in self.selected.keys():
                obj = get_element_by_id_from_doc(obj_id, template)
                if obj is not None:
                    obj.getparent().remove(obj)
        return template

    def copy_doc_with_common_layers(self):
        """Return copy of document with only common layers for export."""
        template = deepcopy(self.document)
        for layer in layer_objs(self.document):
            if layer not in self.commonlayers:
                obj = get_element_by_id_from_doc(layer.get('id'), template)
                if obj is not None:
                    obj.getparent().remove(obj)
        return template

    def get_template(self):
        if self.options.selection_other:
            template = self.copy_doc_with_unselected()
        elif self.options.layer_other:
            template = self.copy_doc_with_common_layers()
        elif self.options.sublayer_other:
            template = self.copy_doc_with_common_layers()
        else:
            template = self.copy_doc_empty()
        # adjust common settings in template
        namedview = template.find(inkex.addNS('namedview', 'sodipodi'))
        if namedview is not None:
            if self.options.background == 'custom':
                color, opacity = int_to_color(self.options.background_color)
                set_pagecolor(namedview, color, opacity)
            elif self.options.background != "default":
                namedview.set(inkex.addNS('pageopacity', 'inkscape'),
                              self.options.background)
        return template

    def obj_index_backwards(self, node, document, mode='"selection"'):
        """Return index to insert node in parent (reversed lookup)."""
        index = None
        if ((mode == '"selection"' and len(self.selected) > 1) or
                (mode != '"selection"' and self.commonlayers)):
            passed = False
            prev_other = None
            next_other = None
            if mode == '"selection"':
                obj_iterator = node.getparent().iterchildren(reversed=True)
            else:
                obj_iterator = layer_objs(self.document, rev=True)
            for obj in obj_iterator:
                if ((mode == '"selection"' and obj not in self.objects) or
                        (mode != '"selection"' and self.commonlayers)):
                    if not passed:
                        next_other = obj
                    else:
                        prev_other = obj
                        break
                if obj == node:
                    passed = True
            if prev_other is not None:
                prev_obj = get_element_by_id_from_doc(
                    prev_other.get('id'), document)
                if prev_obj is not None:
                    index = prev_obj.getparent().index(prev_obj) + 1
            elif next_other is not None:
                next_obj = get_element_by_id_from_doc(
                    next_other.get('id'), document)
                if next_obj is not None:
                    index = next_obj.getparent().index(next_obj)
        return index

    def obj_index_forwards(self, node, document, mode='"selection"'):
        """Return index to insert node in parent in template."""
        index = None
        if ((mode == '"selection"' and len(self.selected) > 1) or
                (mode != '"selection"' and self.commonlayers)):
            passed = False
            prev_other = None
            next_other = None
            if mode == '"selection"':
                obj_iterator = node.getparent().iterchildren()
            else:  # mode != '"selection"'
                obj_iterator = layer_objs(self.document)
            for obj in obj_iterator:
                if ((mode == '"selection"' and obj not in self.objects) or
                        (mode != '"selection"' and obj in self.commonlayers)):
                    if not passed:
                        prev_other = obj
                    else:
                        next_other = obj
                        break
                if obj == node:
                    passed = True
            if prev_other is not None:
                prev_obj = get_element_by_id_from_doc(
                    prev_other.get('id'), document)
                if prev_obj is not None:
                    index = prev_obj.getparent().index(prev_obj) + 1
            elif next_other is not None:
                next_obj = get_element_by_id_from_doc(
                    next_other.get('id'), document)
                if next_obj is not None:
                    index = next_obj.getparent().index(next_obj)
        return index

    def obj_index(self, node, document, mode='"selection"'):
        """Return index to insert node in parent in template."""
        if self.options.incremental:
            return self.obj_index_backwards(node, document, mode)
        else:
            return self.obj_index_forwards(node, document, mode)

    def obj_to_doc(self, document, i, node):
        """Insert or append a copy of node into template document."""
        # pylint: disable=too-many-branches
        counter = str(self.options.counter_start + i).zfill(
            self.options.counter_zfill)
        name = '{0}{1}'.format(self.options.basename, counter)
        if self.options.nb_scope == '"selection"':
            if self.options.selection_other:
                # get index and insert/append copy of node
                index = self.obj_index(node, document, self.options.nb_scope)
                if index is not None:
                    get_element_by_id_from_doc(
                        node.getparent().get('id'),
                        document).insert(index, deepcopy(node))
                else:
                    get_element_by_id_from_doc(
                        node.getparent().get('id'),
                        document).append(deepcopy(node))
            else:
                # wrap copy of node in new layer
                document.getroot().append(
                    wrap_copy_in_new_layer(node, self.options.basename))
            add_label_to_copy(node, document, counter)
            if self.options.selection_fitpage:
                self.fit_page_to_obj(document, node)
        elif (self.options.nb_scope == '"layer"' or
              self.options.nb_scope == '"sublayer"'):
            new_node = deepcopy(node)
            # set layer style
            style = simplestyle.parseStyle(new_node.get('style'))
            # reset layer opacity (optional)
            if self.options.layer_reset:
                style['opacity'] = '1'
            # make layer visible (unconditional)
            style['display'] = 'inline'
            new_node.set('style', simplestyle.formatStyle(style))
            # get index and insert/append copy of node
            if self.options.nb_scope == '"sublayer"':
                obj = get_toplevel_layer(node)
                # compensate transforms of omitted parent layers
                if node.getparent().getparent() is not None:
                    mat = simpletransform.composeParents(
                        node.getparent(), IDENT_MAT)
                    simpletransform.applyTransformToNode(mat, new_node)
            else:
                obj = node
            index = self.obj_index(obj, document, self.options.nb_scope)
            if index is not None:
                document.getroot().insert(index, new_node)
            else:
                document.getroot().append(new_node)
            if self.options.layer_fitpage or self.options.sublayer_fitpage:
                self.fit_page_to_obj(document, node)
        update_docname(document, name)
        return document

    def render_to_png(self, template, export_tmp_target):
        """Export SVG and render to PNG, return list with PNG filepaths."""
        inkshell_cmd = ""
        png_files = []
        document = deepcopy(template)
        for i, obj in enumerate(self.objects):
            if self.options.incremental:
                document = self.obj_to_doc(document, i, obj)
            else:
                document = self.obj_to_doc(deepcopy(template), i, obj)
            filepath = write_svg_file(export_tmp_target, document)
            pngpath = os.path.splitext(filepath)[0] + '.png'
            inkshell_cmd += '--file="{0}" '.format(filepath)
            inkshell_cmd += '--export-dpi={0} '.format(
                self.options.png_resolution)
            inkshell_cmd += '--export-png="{0}"\n'.format(pngpath)
            png_files.append(pngpath)
        inkscape_shell(inkshell_cmd)
        return png_files

    def convert_to_plain_svg(self, template, export_tmp_target):
        """Export SVG, convert to Plain SVG, return list with SVG filepaths."""
        inkshell_cmd = ""
        svg_files = []
        document = deepcopy(template)
        for i, obj in enumerate(self.objects):
            if self.options.incremental:
                document = self.obj_to_doc(document, i, obj)
            else:
                document = self.obj_to_doc(deepcopy(template), i, obj)
            filepath = write_svg_file(export_tmp_target, document)
            svgpath = os.path.splitext(filepath)[0] + '-plain.svg'
            inkshell_cmd += '--file="{0}" '.format(filepath)
            if self.options.svg_text_outline:
                inkshell_cmd += '--export-text-to-path '
            inkshell_cmd += '--export-plain-svg="{0}"\n'.format(svgpath)
            svg_files.append(svgpath)
        inkscape_shell(inkshell_cmd)
        return svg_files

    def convert_to_pdf(self, template, export_tmp_target):
        """Export SVG and convert to PDF, return list with PDF filepaths."""
        inkshell_cmd = ""
        pdf_files = []
        document = deepcopy(template)
        for i, obj in enumerate(self.objects):
            if self.options.incremental:
                document = self.obj_to_doc(document, i, obj)
            else:
                document = self.obj_to_doc(deepcopy(template), i, obj)
            filepath = write_svg_file(export_tmp_target, document)
            pdfpath = os.path.splitext(filepath)[0] + '.pdf'
            inkshell_cmd += '--file="{0}" '.format(filepath)
            if self.options.pdf_text_outline:
                inkshell_cmd += '--export-text-to-path '
            if not self.options.pdf_filter_raster:
                inkshell_cmd += '--export-ignore-filters '
            else:
                inkshell_cmd += '--export-dpi={0} '.format(
                    self.options.pdf_resolution)
            inkshell_cmd += '--export-pdf="{0}"\n'.format(pdfpath)
            pdf_files.append(pdfpath)
        inkscape_shell(inkshell_cmd)
        return pdf_files

    def convert_to_gif(self, png_files):
        """Convert PNG files to GIFs, return list with GIF filepaths."""
        # pylint: disable=no-self-use
        gif_files = []
        for img_filepath in png_files:
            opts = ['convert']
            opts += [img_filepath]
            if self.options.gif_interlace:
                opts += ['-interlace', 'Line']
            opts += [os.path.splitext(img_filepath)[0] + '.gif']
            run_with_shell_if_needed(opts)
            gif_files.append(opts[-1])
        return gif_files

    def convert_to_jpg(self, png_files):
        """Convert PNG files to JPEGs, return list with JPEG filepaths."""
        # pylint: disable=no-self-use
        jpg_files = []
        for img_filepath in png_files:
            opts = ['convert']
            opts += [img_filepath]
            opts += ['-quality', str(self.options.jpg_quality)+'%']
            if self.options.jpg_interlace:
                opts += ['-interlace', 'Line']
            opts += [os.path.splitext(img_filepath)[0] + '.jpg']
            run_with_shell_if_needed(opts)
            jpg_files.append(opts[-1])
        return jpg_files

    def save_anim_gif_cmd(self, anim_filename, opts, tmp_target):
        """Save command to create animated GIF."""
        # pylint: disable=unused-argument
        info = ""
        cmd_output_file = None
        # TODO: write command(s) to separate file (script)
        # - filepaths: absolute or relative?
        #   (abs not re-usable, since files in temp dir are cleaned up)
        # - cross-platform (shell or batch script depending on OS)?
        # - optionally append command to file (e.g. 2nd call to optimize)
        # copy file with IM command(s) to export path
        if os.path.isfile(cmd_output_file):
            path = check_filepath(self.options.export_path)
            if path is not None:
                copy_file_to('files', path, cmd_output_file)
                info += '\nAnimated GIF cmd file saved as: {0}'.format(
                    os.path.join(path, os.path.basename(cmd_output_file)))
        return info

    def optimize_animated_gif(self, anim_filename, output_file, tmp_target):
        """Optimized default animated GIF via ImageMagick."""
        info = ""
        opt_output_file = os.path.join(
            tmp_target, '{0}_opt.gif'.format(anim_filename))
        oopts = ['convert']
        oopts += [output_file]
        oopts += ['-layers', 'Optimize']
        oopts += [opt_output_file]
        run_with_shell_if_needed(oopts, verbose=False)
        # copy optimized animated image to export path
        if os.path.isfile(opt_output_file):
            path = check_filepath(self.options.export_path)
            if path is not None:
                copy_file_to('files', path, opt_output_file)
                info += '\nAnimated GIF optimized as: {0}'.format(
                    os.path.join(path, os.path.basename(opt_output_file)))
        # append IM command to file
        if self.options.anim_gif_cmd:
            info += self.save_anim_gif_cmd(
                anim_filename, oopts, tmp_target)
        return info

    def create_animated_gif(self, img_files, tmp_target):
        """Create animated image from exported bitmaps."""
        info = ""
        if self.options.anim_gif_filename:
            anim_filename = self.options.anim_gif_filename
        else:
            anim_filename = '{0}_anim'.format(self.options.basename)
        output_file = os.path.join(tmp_target, '{0}.gif'.format(anim_filename))
        opts = ['convert']
        opts += ['-delay', '{0}x1000'.format(self.options.anim_gif_delay)]
        opts += ['-loop', str(self.options.anim_gif_loop)]
        opts += ['-dispose', self.options.anim_gif_dispose]
        opts += img_files
        opts += [output_file]
        run_with_shell_if_needed(opts, verbose=False)
        # copy animated image to export path
        if os.path.isfile(output_file):
            path = check_filepath(self.options.export_path)
            if path is not None:
                copy_file_to('files', path, output_file)
                info += '\nAnimated GIF saved as: {0}'.format(
                    os.path.join(path, os.path.basename(output_file)))
        # write IM command to file
        if self.options.anim_gif_cmd:
            info += self.save_anim_gif_cmd(
                anim_filename, opts, tmp_target)
        # optimize the animated GIF
        if self.options.anim_gif_optim:
            info += self.optimize_animated_gif(
                anim_filename, output_file, tmp_target)
        return info

    def export_to_svg(self, template, export_target):
        """Export objects into template to export_target."""
        info = ""
        export_tmp_target = tempfile.mkdtemp()
        if self.options.svg_plain:
            svg_files = self.convert_to_plain_svg(template, export_tmp_target)
        else:
            svg_files = []
            document = deepcopy(template)
            for i, obj in enumerate(self.objects):
                if self.options.incremental:
                    document = self.obj_to_doc(document, i, obj)
                else:
                    document = self.obj_to_doc(deepcopy(template), i, obj)
                filepath = write_svg_file(export_tmp_target, document)
                svg_files.append(filepath)
        out_count = copy_filelist_to(
            svg_files, self.options.export_type, export_target)
        info += self.out_info(out_count)
        clear_tmp(export_tmp_target)
        return info

    def export_to_png(self, template, export_target):
        """Export objects into template, to PNG in export_target."""
        info = ""
        export_tmp_target = tempfile.mkdtemp()
        png_files = self.render_to_png(template, export_tmp_target)
        out_count = copy_filelist_to(
            png_files, self.options.export_type, export_target)
        info += self.out_info(out_count)
        if HAVE_CONVERT:
            if self.options.anim_gif:
                info += self.create_animated_gif(png_files, export_tmp_target)
        clear_tmp(export_tmp_target)
        return info

    def export_to_gif(self, template, export_target):
        """Export objects into template, to PNG in export_target."""
        info = ""
        if HAVE_CONVERT:
            export_tmp_target = tempfile.mkdtemp()
            gif_files = self.convert_to_gif(
                self.render_to_png(template, export_tmp_target))
            out_count = copy_filelist_to(
                gif_files, self.options.export_type, export_target)
            info += self.out_info(out_count)
            if self.options.anim_gif:
                info += self.create_animated_gif(gif_files, export_tmp_target)
            clear_tmp(export_tmp_target)
        else:
            self.objects = []
            info = '\nExport format {0} not available.'.format(
                self.options.export_format.upper())
        return info

    def export_to_jpg(self, template, export_target):
        """Export objects into template, to JPEG in export_target."""
        info = ""
        if HAVE_CONVERT:
            export_tmp_target = tempfile.mkdtemp()
            jpg_files = self.convert_to_jpg(
                self.render_to_png(template, export_tmp_target))
            out_count = copy_filelist_to(
                jpg_files, self.options.export_type, export_target)
            info += self.out_info(out_count)
            if self.options.anim_gif:
                info += self.create_animated_gif(jpg_files, export_tmp_target)
            clear_tmp(export_tmp_target)
        else:
            self.objects = []
            info = '\nExport format {0} not available.'.format(
                self.options.export_format.upper())
        return info

    def export_to_pdf(self, template, export_target):
        """Export objects into template, to PDF in export_target."""
        info = ""
        export_tmp_target = tempfile.mkdtemp()
        pdf_files = self.convert_to_pdf(template, export_tmp_target)
        out_count = copy_filelist_to(
            pdf_files, self.options.export_type, export_target)
        info += self.out_info(out_count)
        clear_tmp(export_tmp_target)
        return info

    def export_to(self, template, export_target):
        """Dispatcher for export formats."""
        export_cmd = getattr(
            self, 'export_to_{0}'.format(self.options.export_format.lower()))
        if export_cmd is not None:
            info = export_cmd(template, export_target)
        else:
            self.objects = []
            info = '\nUnknown export format: {0}.'.format(
                self.options.export_format)
        return info

    def export_objects(self):
        # pylint: disable=redefined-variable-type
        path = check_filepath(self.options.export_path)
        template = self.get_template()
        self.check_basename()
        self.check_counter()
        if path is not None and template is not None:
            if self.options.export_type == "files":
                export_target = path
                info = 'Export directory: {0}'.format(export_target)
            elif self.options.export_type == "tar":
                export_target = init_tarfile(path, self.options.basename)
                info = 'Tarball: {0}'.format(export_target.name)
            elif self.options.export_type == "zip":
                export_target = init_zipfile(path, self.options.basename)
                info = 'ZIP archive: {0}'.format(export_target.filename)
            else:
                self.objects = []
                info = 'Unknown export type: {0}'.format(
                    self.options.export_type)
            info += self.export_to(template, export_target)
            if (self.options.export_type == "tar" or
                    self.options.export_type == "zip"):
                export_target.close()
            inkex.errormsg(info)

    def get_selection(self):
        """Store list of objects in selection in instance attribute."""
        self.options.layer_other = False
        self.options.sublayer_other = False
        if len(self.selected) == 1:
            firstobject = self.selected[self.options.ids[0]]
            if is_group(firstobject):
                for child in firstobject.iterchildren(reversed=False):
                    self.objects.append(child)
        else:
            for id_ in zSort(self.document.getroot(), self.selected.keys()):
                self.objects.append(self.selected[id_])
        if self.objects and self.options.reverse:
            self.objects.reverse()
        if not self.objects:
            return ("This export mode requires a selection of " +
                    "one group or several objects.")

    def add_layer_to_objects(self, node):
        """Append node to instance attribute 'objects'."""
        if self.options.layer_visible or self.options.sublayer_visible:
            style = simplestyle.parseStyle(node.get('style'))
            if style['display'] != 'none':
                self.objects.append(node)
        else:
            self.objects.append(node)

    def recurse_sublayer_levels(self, node, level, counter=0):
        """Recurse into sublayer levels to add sublayers matching level."""
        counter += 1
        for subnode in sublayer_objs(node, self.options.reverse):
            if counter == level:
                self.add_layer_to_objects(subnode)
            else:
                self.recurse_sublayer_levels(subnode, level, counter)

    def get_sublayers(self):
        """Store list of export sublayers in instance attribute."""
        # pylint: disable=too-many-branches
        info = ""
        self.options.selection_other = False
        self.options.layer_other = False
        if self.options.sublayer_other:
            self.get_common_layers(self.options.sublayer_list)
        current_toplevel = get_toplevel_layer(self.current_layer)
        level = int(self.options.sublayer_level)
        if self.options.sublayer_scope == 'sublayer_current':
            if current_toplevel not in self.commonlayers:
                self.recurse_sublayer_levels(current_toplevel, level)
        elif self.options.sublayer_scope == 'sublayer_all':
            for node in layer_objs(self.document, self.options.reverse):
                if node not in self.commonlayers:
                    self.recurse_sublayer_levels(node, level)
        elif self.options.sublayer_scope == 'sublayer_layer_visible':
            for node in layer_objs(self.document, self.options.reverse):
                if node not in self.commonlayers:
                    style = simplestyle.parseStyle(node.get('style'))
                    if style['display'] != 'none':
                        self.recurse_sublayer_levels(node, level)
        else:
            info += 'Unknown sublayer scope: {0}\n'.format(
                self.options.sublayer_scope)
        if not self.objects:
            if self.options.sublayer_visible:
                info += "This mode requires one or more visible sublayers "
            else:
                info += "This mode requires one or more sublayers "
            return info + "apart from any common ones."

    def get_layers(self):
        """Store list of top-level export layers in instance attribute."""
        self.options.selection_other = False
        self.options.sublayer_other = False
        if self.options.layer_other:
            self.get_common_layers(self.options.layer_list)
        for node in layer_objs(self.document, self.options.reverse):
            if node not in self.commonlayers:
                self.add_layer_to_objects(node)
        if not self.objects:
            if self.options.layer_visible:
                info = "This mode requires one or more visible layers "
            else:
                info = "This mode requires one or more layers "
            return info + "apart from any common ones."

    def get_objects(self):
        if self.options.nb_scope == '"selection"':
            info = self.get_selection()
        elif self.options.nb_scope == '"layer"':
            info = self.get_layers()
        elif self.options.nb_scope == '"sublayer"':
            info = self.get_sublayers()
        else:  # unknown scope
            info = 'Export scope "{0}" not implemented.'.format(
                self.options.nb_scope)
        if info:
            inkex.errormsg(info)
        return len(self.objects)

    def check_option_conflicts(self, verbose=True):
        """Check for conflicting options and warn user."""
        msg = ""
        conflicts = False
        # fit to page
        if (self.options.selection_fitpage or
                self.options.layer_fitpage or
                self.options.sublayer_fitpage):
            if (self.options.anim_gif and
                    self.options.export_format in ('png', 'gif', 'jpg')):
                msg = ("'Fit page to ...' conflicts with " +
                       "'Create animated GIF' " +
                       "when exporting to a bitmap format.")
                conflicts = True
        # output and return
        if verbose and msg:
            inkex.errormsg(msg)
        return conflicts

    def effect(self):
        # check options first
        if not self.check_option_conflicts():
            # support 90 and 96 dpi
            self.set_dpi(dpi='default')
            # export objects
            if self.get_objects():
                self.export_objects()


if __name__ == '__main__':
    ME = ExportObjects()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
